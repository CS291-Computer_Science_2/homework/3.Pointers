/*
	Original Date: 20160927
	Modified Date: 20180607
*/

#include <iostream>
using namespace std;

void dblIt(int x, int *p){
	*p = 2*x;
}

void vswap(int &a, int &b){
	swap(a,b);
}

int main() {	
	//swap numbers with references
	int a=20,b=87;	
	cout<<"(Before Swap) a: "<<a<<" b: "<<b<<endl;
	vswap(a,b);	
	cout<<"(After Swap) a: "<<a<<" b: "<<b<<endl;
	
	//double a number using pointer reference
	int x = 33;
	int *p = &x;
	cout<<"\nOriginal: "<<x<<endl;
	dblIt(x, p);
	cout<<"The Double is: "<<*p<<endl;

	system("pause");
	return 0;
}


